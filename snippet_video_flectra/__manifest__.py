# Part of Flectra Module Developed by 73lines
# See LICENSE file for full copyright and licensing details.

{
    'name': 'Audio Video Upload Snippet',
    'summary': 'Audio Video Upload Snippet',
    'version': '1.0',
    'author': 'Flectra Community',
    'license': "AGPL-3",
    'maintainer': 'Flectra Community',
    'category': 'Website',
    'website': 'https://www.flectra-community.org',
    'depends': ['website','web_editor'],
    'data': [
        'views/assets.xml'
    ],
    'qweb': [
        'static/src/xml/qweb.xml',
    ],
    'demo': [],
    'installable': True,
    'auto_install': False,
    'application': True,
}
