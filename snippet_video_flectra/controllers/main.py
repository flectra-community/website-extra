# Part of Flectra Module Developed by 73lines
# See LICENSE file for full copyright and licensing details.

from flectra import http
from flectra.http import request
import base64
import json
import io


class WebsiteUploadVideo(http.Controller):

    @http.route('/snippet_video_flectra/attach',
                type='http', auth='user', methods=['POST'], website=True, csrf=False)
    def attach(self, url=None, upload=None, **kwargs):
        Attachments = request.env['ir.attachment']
        attachments = request.env['ir.attachment']
        video_data = upload.read()
        # data = video_data.read()
        uploads = []

        res_model = kwargs.get('res_model', 'ir.ui.view')
        if res_model != 'ir.ui.view' and kwargs.get('res_id'):
            res_id = int(kwargs['res_id'])
        else:
            res_id = None

        attachment_id = Attachments.create({
            'name': upload.filename,
            'datas': base64.b64encode(video_data),
            'datas_fname': upload.filename,
            'public': res_model == 'ir.ui.view',
            'res_id': res_id,
            'res_model': res_model,
        })
        attachment_id.generate_access_token()
        attachments += attachment_id
        uploads += attachments.read(
            ['name', 'mimetype', 'checksum', 'res_id', 'res_model',
             'access_token'])
        return """<script type='text/javascript'>
                    window.parent['video_upload_callback'](%s);
                </script>""" % (json.dumps(uploads))
