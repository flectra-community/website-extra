===========
Audio Video Upload Snippet
===========

This module adds the feature to upload a existing audio or video file.

**Table of contents**

.. contents::
   :local:

Usage
=====

To use this module, you need to:

#. open website
#. edit website
#. paste any existing image block
#. double click image
#. open video tab and upload your audio or video file

Credits
=======

Authors
~~~~~~~

* Flectra Community

Contributors
------------

* Flectra Community <info@flectra-community.org>
