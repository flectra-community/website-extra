flectra.define('snippet_video_flectra.video', function (require) {
"use strict";

    var website = require('web_editor.base');
    var core = require('web.core');
    var _t = core._t;
    var ajax = require('web.ajax');
    var qweb = core.qweb;
    ajax.loadXML('/snippet_video_flectra/static/src/xml/qweb.xml', qweb);
    var Dialog = require('web.Dialog');
    var widget = require('web_editor.widget');
    var base = require('web_editor.base');
    var utils = require('web.utils');
    var weContext = require("web_editor.context");
    var QWeb = core.qweb;

       widget.VideoDialog.include({
    events: {
        'click .filepicker': '_onClick',
        'change input[type=file]': 'video_selected',
    },

    _onClick: function(){
        var filepicker = this.$('input[type=file]');
        if (!_.isEmpty(filepicker)){
            filepicker[0].click();
        }
    },

    video_selected: function(e) {
        var self = this;
        var $input = $(e.target);
        var $form = $input.parent();
        self.toggleButton();
        window['video_upload_callback'] = function(id) {
            self.set('attachment_id', id);
            delete window['video_upload_callback'];
            self.toggleButton();
            self.showMessage();
        }
        $form.submit();

    },


    showMessage: function() {
            var span = this.$el.find('.o_website_upload_success');
            span.removeClass('hidden');
            span.addClass('show');
    },


    hideMessage: function() {
            var span = this.$el.find('.o_website_upload_success');
            span.removeClass('show');
            span.addClass('hidden');
    },
    toggleButton: function() {
            var btn = this.$el.find('.o_website_upload_video_btn');
            if(btn.hasClass('disabled')) {
                btn.removeClass('disabled').html(_t('Upload a video'));
            } else {
                btn.addClass('disabled', 'disabled').html(_t('Uploading...'));
            }
    },
    save: function() {
           var attach_id = this.get('attachment_id')
           var attach_ids = attach_id.map(function (val) {
                return val.id;

            });

            var attach_ids_video = attach_id.map(function (val) {
                return val.mimetype;

            });

            var attach_ids_audio = attach_id.map(function (val) {
                return val.mimetype;

            });


            if(attach_ids_video == 'video/mp4'|| attach_ids_video == 'video/ogg'|| attach_ids_video == 'video/webm') {
                if(this.get('attachment_id') != null) {
                    var $video = $(
                        '<video width="100%" height="100%" controls>' +
                            '<source src="/web/content/?model=ir.attachment&field=datas&id='+ attach_ids +'" type="video/mp4">' +
                        '</video>'
                    );
                    $(this.media).replaceWith($video);
                    this.media = $video[0];
                    return this.media;
                    this._super();
                }
            }

            if(attach_ids_audio == 'audio/mpeg'|| attach_ids_video == 'audio/ogg'|| attach_ids_audio == 'audio/x-wav') {
                if(this.get('attachment_id') != null) {
                    var $audio = $(
                        '<audio width="100%" height="100%" controls preload>' +
                            '<source src="/web/content/?model=ir.attachment&field=datas&id='+ attach_ids +'" type="audio/mp3">' +
                        '</audio>'
                    );
                    $(this.media).replaceWith($audio);
                    this.media = $audio[0];
                    return this.media;
                    this._super();
                }
            }

    }

    });

    widget.ImageDialog.include({
        save: function() {
            var self = this;
            var old_media = self.old_media;
            var o_media = self.images;
            var mimetype = o_media["0"].mimetype
            var id = o_media["0"].id
            var media = self.media;

            if(mimetype == 'video/mp4'|| mimetype == 'video/ogg'|| mimetype == 'video/webm') {
                var $video = $(
                    '<video width="100%" height="100%" controls>' +
                        '<source src="/web/content/?model=ir.attachment&field=datas&id='+ id +'" type="video/mp4">' +
                    '</video>'
                );
                $(this.media).replaceWith($video);
                this.media = $video[0];
                return this.media;
            }

             if(mimetype == 'audio/mpeg'|| mimetype == 'audio/ogg' || mimetype == 'audio/x-wav') {
                    var $audio = $(
                        '<audio width="100%" height="100%" controls preload>' +
                            '<source src="/web/content/?model=ir.attachment&field=datas&id='+ id +'" type="audio/mp3">' +
                        '</audio>'
                    );
                    $(this.media).replaceWith($audio);
                    this.media = $audio[0];
                    return this.media;
            }

            this._super();

        },



    });

});
